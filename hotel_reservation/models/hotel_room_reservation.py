from odoo import models, fields, api, _
from odoo.tools import DEFAULT_SERVER_DATETIME_FORMAT as dt
from odoo.exceptions import except_orm, ValidationError
from dateutil.relativedelta import relativedelta
from datetime import datetime, timedelta
from odoo.fields import Datetime as odooDatetime

import pytz
import math

import logging
_logger = logging.getLogger(__name__)

LEAVE_HOUR = 13
LEAVE_MINUTE = 0
EXTENDED_HOURS = 8


class hotel_room_reservation_tag(models.Model):
    _name = 'hotel.room_reservation.tag'
    _description = 'hotel reservation room tag'

    name = fields.Char(
        string='Name',
    )


class hotel_room_reservation(models.Model):
    _name = 'hotel.room_reservation'
    _description = 'hotel reservation room'
    _inherit = ['mail.thread', 'ir.needaction_mixin']

    name = fields.Char(
        string='Reservation No',
        size=64,
        required=False,
        default='/'
    )
    day_amount = fields.Float(
        string='day',
    )
    days_qty = fields.Float(
        string='Days',
    )
    amount = fields.Float(
        string='Total',
        compute='total_amount'
    )
    pay_journal_id = fields.Many2one(
        'account.journal',
        string='Metodo de pago',
    )

    pay_amount = fields.Float(
        string='Monto',
    )

    date_order = fields.Datetime('Date Ordered', readonly=True, required=True,
                                 index=True,
                                 default=odooDatetime.now()
    )

    warehouse_id = fields.Many2one('stock.warehouse', 'Hotel',
                                   index=True,
                                   required=True,
                                   default=lambda self: self.env[
                                       'stock.warehouse'].search([], limit=1).id
    )
    partner_id = fields.Many2one('res.partner', 'Guest Name', readonly=True,
                                 index=True,
                                 required=True,
                                 states={'draft': [('readonly', False)]}
                                 )
    pricelist_id = fields.Many2one('product.pricelist', 'Scheme',
                                   required=True, readonly=True,
                                   states={'draft': [('readonly', False)]},
                                   help="Pricelist for current reservation.")
    """invoice_id = fields.Many2one(
        'account.invoice',
        string='invoice',
    )"""
    invoice_ids = fields.Many2many(
        'account.invoice',
        string='invoice',
    )
    """currency_id = fields.Many2one(
        'res.currency', 
        related='invoice_id.currency_id',
        related_sudo=False
    )
    
    residual = fields.Monetary(
        string='Amount Due',
        related='invoice_id.residual',
        help="Remaining amount due."
    )"""

    invoiced = fields.Float(
        string='Monto  facturado',
        compute="compute_residual",
        store=True,
    )

    residual = fields.Float(
        string='Monto no cobrado',
        compute="compute_residual",
        store=True,
    )

    partner_invoice_id = fields.Many2one('res.partner', 'Invoice Address',
                                         help="Invoice address for "
                                         "current reservation.")
    checkin = fields.Datetime('checkin', required=True,)
    checkout = fields.Datetime('checkout', required=True,)
    adults = fields.Integer('Adults', size=64, readonly=True,
                            states={'draft': [('readonly', False)]},
                            help='List of adults there in guest list. ')
    children = fields.Integer('Children', size=64, readonly=True,
                              states={'draft': [('readonly', False)]},
                              help='Number of children there in guest list.')

    room_id = fields.Many2one(
        'hotel.room',
        string='Room',
    )
    product_id = fields.Many2one(
        'product.template',
        'Product_id',
        related='room_id.product_id'
    )

    room_variant_id = fields.Many2one(
        'product.product',
        'Variant',
        # required=True
    )

    state = fields.Selection([
        ('draft', 'Draft'),
        ('confirm', 'Confirm'),
        ('checkin', 'check in'),
        ('checkout', 'check out'),
        ('cancel', 'Cancel'),
        ('done', 'Done')],
        'State', readonly=True,
        default=lambda *a: 'draft'
    )
    folio_id = fields.Many2one(
        'hotel.folio',
        string='Folio',
    )
    """folio_id = fields.Many2many('hotel.folio', 'hotel_folio_reservation_rel',
                                'order_id', 'invoice_id', string='Folio')"""
    #dummy = fields.Datetime('Dummy')
    notes = fields.Text(
        string='Notas',
    )
    tag_ids = fields.Many2many(
        'hotel.room_reservation.tag',
        string='Tags',
    )

    @api.constrains('checkin', 'checkout', 'room_id')
    def is_overlap(self):
        overlap = self.search([
            ('checkin', '<', self.checkout),
            ('checkout', '>', self.checkin),
            ('id', '!=', self.id),
            ('room_id', '=', self.room_id.id),
            ('state', 'in', ['confirm', 'check_in'])
        ])
        if len(overlap):
            raise ValidationError('Hay una reserva confirmada para esa fecha')

    @api.one
    def action_cancel(self):
        self.state = 'cancel'

    @api.one
    def action_confirm(self):
        self.action_invoice()
        self.state = 'confirm'

    @api.one
    def action_checkin(self):
        self.state = 'checkin'

    @api.one
    def action_checkout(self):
        self.state = 'checkout'

    @api.onchange('partner_id')
    def _onchange_partner_id(self):
        if len(self.partner_id):
            self.partner_invoice_id = self.partner_id.id
            self.pricelist_id = self.partner_id.property_product_pricelist.id
        else:
            self.partner_invoice_id = False
            self.pricelist_id = False

    @api.onchange('checkout', 'checkin', 'room_variant_id')
    def on_change_check_out(self):
        if self.checkout and self.checkin:
            if self.checkout < self.checkin:
                raise except_orm(_('Warning'),
                                 _('La Fecha de ingreso debe ser menor a la de egreso'))
        if self.checkout and self.checkin and len(self.room_variant_id):

            diff = odooDatetime.from_string(
                self.checkout) - odooDatetime.from_string(self.checkin)
            day_diff = float(diff.days) + float(diff.seconds) / 24 / 60 / 60
            self.day_amount = self.room_variant_id.lst_price
            #self.days_qty = math.ceil(day_diff) *1.0
            self.days_qty = math.ceil(day_diff * 4.0) / 4.0

    @api.onchange('day_amount', 'days_qty')
    @api.multi
    def total_amount(self):
        for reservation in self:
            if reservation.checkin and reservation.days_qty:
                if reservation.days_qty.is_integer():

                    reservation.checkout = fields.Datetime.to_string((odooDatetime.from_string(reservation.checkin) + relativedelta(days=reservation.days_qty)).replace(
                        hour=LEAVE_HOUR,
                        minute=LEAVE_MINUTE,
                        second=0,
                        microsecond=0,

                        # tzinfo=timezone
                    ))

                reservation.amount = reservation.day_amount * reservation.days_qty

    @api.model
    def default_get(self, fields):
        """
        To get default values for the object.
        @param self: The object pointer.
        @param fields: List of fields for which we want default values
        @return: A dictionary which of fields with values.
        """
        if self._context is None:
            self._context = {}
        res = super(hotel_room_reservation, self).default_get(fields)
        if self._context:
            keys = self._context.keys()
        #_logger.info("tz %r" %self.env.user.tz)
        #timezone = pytz.timezone(self._context.get('tz', False))

        if 'date' in keys:
            check_in = odooDatetime.to_string(
                odooDatetime.from_string(self._context['date']).replace(
                    hour=LEAVE_HOUR,
                    minute=LEAVE_MINUTE + 1,
                    second=0,
                    microsecond=0,

                    # tzinfo=timezone
                )
            )

            check_out = odooDatetime.to_string(
                odooDatetime.from_string(self._context['date']).replace(
                    hour=LEAVE_HOUR,
                    minute=LEAVE_MINUTE,
                    second=0,
                    microsecond=0,
                    # tzinfo=timezone
                ) + timedelta(days=1)
            )
            res.update({
                'checkin': check_in,
                'checkout': check_out
            })
        if 'room_id' in keys:

            wh_id = self.env['stock.warehouse'].search([], limit=1)
            room_id = self.env['hotel.room'].browse(
                int(self._context['room_id']))
            res.update({
                'warehouse_id': wh_id.id,
                'room_id': room_id.id,
                'room_variant_id': room_id.product_id.product_variant_ids[0].id
            })
        return res

    @api.model
    def create(self, vals):

        vals['name'] = self.env['ir.sequence'].get('hotel.reservation')
        return super(hotel_room_reservation, self).create(vals)

    @api.multi
    def res_checkdate(self, date):
        date_in = odooDatetime.from_string(date).replace(
            hour=LEAVE_HOUR, minute=LEAVE_MINUTE + 1)
        date_out = odooDatetime.from_string(date).replace(
            hour=LEAVE_HOUR, minute=LEAVE_MINUTE) + timedelta(days=1)
        rtn = []
        for reserv in self:
            checkin = odooDatetime.from_string(reserv.checkin)
            checkout = odooDatetime.from_string(reserv.checkout)
            if date_in <= checkout and date_out >= checkin:
                # return reserv
                rtn.append(reserv)
        return rtn

    @api.one
    @api.depends('invoice_ids', 'amount')
    def compute_residual(self):
        residual = 0.0
        invoiced = 0.0

        for invoice_id in self.invoice_ids:
            invoiced += invoice_id.amount_total
            residual += invoice_id.amount_total - invoice_id.residual
        self.invoiced = invoiced
        self.residual = self.amount - residual

    @api.one
    def action_invoice_and_pay(self):
        if self.pay_amount > 0:

            inv = {}
            inv['partner_id'] = self.partner_invoice_id.id
            inv['account_id'] = self.partner_invoice_id.property_account_receivable_id.id
            inv['invoice_line_ids'] = []
            inv['journal_id'] = self.env.ref(
                'hotel_reservation.rooms_journal').id
            inv['comment'] = self.notes
            account_id = self.room_variant_id.property_account_income_id or self.room_variant_id.categ_id.property_account_income_categ_id

            inv['invoice_line_ids'].append((0, 0, {
                'product_id': self.room_variant_id.id,
                'name': self.room_variant_id.name,
                'account_id': account_id.id,
                'quantity': 1,
                'price_unit': self.pay_amount
            }))
            invoice_id = self.env['account.invoice'].create(inv)

            self.invoice_ids = [(4, invoice_id.id)]
            self.state = 'confirm'

            invoice_id.action_invoice_open()

            ctx = {'active_model': 'account.invoice',
                   'active_ids': [invoice_id.id]}

            register_payments = self.env['account.register.payments'].with_context(ctx).create({
                'payment_date': fields.Date.today(),
                'journal_id': self.pay_journal_id.id,
                'payment_method_id': self.env.ref("account.account_payment_method_manual_in").id,
            })

            register_payments.amount = self.pay_amount
            register_payments.create_payment()
            self.pay_amount = 0

    @api.one
    def action_invoice(self):

        inv = {}
        inv['partner_id'] = self.partner_invoice_id.id
        inv['account_id'] = self.partner_invoice_id.property_account_receivable_id.id
        inv['invoice_line_ids'] = []
        inv['journal_id'] = self.env.ref('hotel_reservation.rooms_journal').id
        inv['comment'] = self.notes
        account_id = self.room_variant_id.property_account_income_id or self.room_variant_id.categ_id.property_account_income_categ_id

        inv['invoice_line_ids'].append((0, 0, {
            'product_id': self.room_variant_id.id,
            'name': self.room_variant_id.name,
            'account_id': account_id.id,
            'quantity': self.days_qty,
            'price_unit': self.day_amount
        }))

        invoice_id = self.env['account.invoice'].create(inv)
        self.invoice_id = invoice_id
        self.state = 'confirm'
        if self.pay_amount > 0:
            invoice_id.action_invoice_open()
            ctx = {'active_model': 'account.invoice',
                   'active_ids': [invoice_id.id]}

            register_payments = self.env['account.register.payments'].with_context(ctx).create({
                'payment_date': fields.Date.today(),
                'journal_id': self.pay_journal_id.id,
                'payment_method_id': self.env.ref("account.account_payment_method_manual_in").id,
            })

            register_payments.amount = self.pay_amount

            register_payments.create_payment()


class hotel_room(models.Model):
    _inherit = 'hotel.room'

    room_reservation_ids = fields.One2many(
        'hotel.room_reservation',
        'room_id',
        string='Reservation',
    )


class hotel_room_reservation_summary(models.Model):

    _name = 'hotel.room_reservation.summary'
    _description = 'Room reservation summary'

    name = fields.Char('Reservation Summary', default='Reservations Summary',
                       invisible=True)
    date_from = fields.Datetime(
        'Date From', 
        default=lambda self:self.env['ir.config_parameter'].sudo().get_param('summary_from_%s'%self._uid , fields.Date.today()))
    date_to = fields.Datetime(
        'Date To', 
        default=lambda self: self.env['ir.config_parameter'].sudo().get_param('summary_to_%s'%self._uid , self.next_weeks())) 
    summary_header = fields.Text('Summary Header', compute='get_room_summary')
    room_summary = fields.Text('Room Summary', compute='get_room_summary')

    @api.one
    def seven_days(self):
        self.env['ir.config_parameter'].sudo().set_param(
            'days_%s'%self._uid ,7)
        self.set_default_period()
    @api.one
    def f_days(self):
        self.env['ir.config_parameter'].sudo().set_param(
            'days_%s'%self._uid ,14)
        self.set_default_period()

    @api.one
    def month(self):
        self.env['ir.config_parameter'].sudo().set_param(
            'days_%s'%self._uid ,30)
        self.set_default_period()

    @api.one
    def set_default_period(self):
        self.date_from = fields.Date.today()
        self.date_to =self.next_weeks()

    @api.model
    def next_weeks(self):
        d = int(self.env['ir.config_parameter'].sudo().get_param('days_%s'%self._uid , 7))
        return fields.Date.to_string(
            fields.Date.from_string(fields.Date.today()) + timedelta(days=d)
        )

    @api.one
    def next(self):
        d = int(self.env['ir.config_parameter'].sudo().get_param('days_%s'%self._uid , 7))

        self.date_from = fields.Date.to_string(
            fields.Date.from_string(self.date_from) + timedelta(days=d)
        )
        self.date_to = fields.Date.to_string(
            fields.Date.from_string(self.date_to) + timedelta(days=d)
        )
        self.env['ir.config_parameter'].sudo().set_param(
            'summary_from_%s'%self._uid , self.date_from)
        self.env['ir.config_parameter'].sudo().set_param(
            'summary_to_%s'%self._uid , self.date_to)

        # self.get_room_summary()

    @api.one
    def prev(self):
        d = int(self.env['ir.config_parameter'].sudo().get_param('days_%s'%self._uid , 7))
        self.date_from = fields.Date.to_string(
            fields.Date.from_string(self.date_from) - timedelta(days=d)
        )
        self.date_to = fields.Date.to_string(
            fields.Date.from_string(self.date_to) - timedelta(days=d)
        )
        self.env['ir.config_parameter'].sudo().set_param(
            'summary_from_%s'%self._uid , self.date_from)
        self.env['ir.config_parameter'].sudo().set_param(
            'summary_to_%s'%self._uid , self.date_to)

        # self.get_room_summary()

   
    @api.onchange('date_from', 'date_to')
    def get_room_summary(self):
        self.env['ir.config_parameter'].sudo().set_param(
            'summary_from_%s'%self._uid , self.date_from)
        self.env['ir.config_parameter'].sudo().set_param(
            'summary_to_%s'%self._uid , self.date_to)

        res = {}
        summary_header_list = ['Rooms']
        date_range_list = []
        all_room_detail = []
        if self._context.get('tz', False):
            timezone = pytz.timezone(self._context.get('tz', False))
        else:
            timezone = pytz.timezone('UTC')
        d_frm_obj = datetime.strptime(self.date_from, dt)\
            .replace(tzinfo=pytz.timezone('UTC')).astimezone(timezone)
        d_to_obj = datetime.strptime(self.date_to, dt)\
            .replace(tzinfo=pytz.timezone('UTC')).astimezone(timezone)
        temp_date = d_frm_obj

        while(temp_date <= d_to_obj):
            val = ''
            val = (str(temp_date.strftime("%a %b %d")).decode('utf-8', 'ignore'))
            summary_header_list.append(val)
            date_range_list.append(temp_date.strftime
                                   (dt))
            temp_date = temp_date + timedelta(days=1)

        room_ids = self.env['hotel.room'].search([])
        room_reserv = {}

        for room in room_ids:
            room_detail = {'name': room.name}
            room_list_stats = []

            reserv_ids = self.env['hotel.room_reservation'].search([
                ('checkin', '<=', self.date_to),
                ('checkout', '>=', self.date_from),
                ('room_id', '=', room.id),
                ('state', 'not in', ['cancel'])
            ])

            for chk_date in date_range_list:
                reserves = reserv_ids.res_checkdate(chk_date)
                if len(reserves):
                    room_day = []

                    for reserv_id in reserves:
                        date_out_extended = odooDatetime.from_string(chk_date).replace(
                            hour=LEAVE_HOUR + EXTENDED_HOURS, minute=LEAVE_MINUTE)
                        if odooDatetime.from_string(reserv_id.checkout) < date_out_extended:

                            room_day.append({'state': 'Parcial',
                                             'name': '%s %s %s' % (
                                                 reserv_id.state,
                                                 reserv_id.name,
                                                 reserv_id.partner_id.name),
                                             'date': chk_date,
                                             'room_id': room.id,
                                             'checkout': reserv_id.checkout,

                                             'is_draft': 'No',
                                             'reservation': reserv_id.id,
                                             'data_model': '',
                                             'color': "color-%s" % reserv_id.state,
                                             'data_id': 0
                                             })

                        else:
                            room_day.append({'state': 'Reserved',
                                             'name': '%s %s %s' % (
                                                 reserv_id.state,
                                                 reserv_id.name,
                                                 reserv_id.partner_id.name),
                                             'date': chk_date,
                                             'room_id': room.id,
                                             'is_draft': 'No',
                                             'checkout': reserv_id.checkout,
                                             'reservation': reserv_id.id,
                                             'data_model': '',
                                             'color': "color-%s" % reserv_id.state,
                                             'data_id': 0
                                             })
                    room_list_stats.append(room_day)
                else:

                    room_list_stats.append([{'state': 'Free',
                                            'date': chk_date,
                                            'room_id': room.id}])
            room_detail.update({'value': room_list_stats})
            all_room_detail.append(room_detail)

        self.summary_header = str([{'header': summary_header_list}])
        self.room_summary = str(all_room_detail)

        return res
