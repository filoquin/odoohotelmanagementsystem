# -*- coding: utf-8 -*-
# See LICENSE file for full copyright and licensing details.

from odoo import fields, models, api


class order_lines_report(models.TransientModel):
    _name = 'order.lines.report'
    _description = 'order lines report'

    from_date = fields.Date(
        string='Desde',
        required=True,
    )

    to_date = fields.Date(
        string='Hasta',
        required=True,
    )

    @api.multi
    def action_get_report(self):
        order_ids = self.env['hotel.restaurant.order'].search(
            [
                ('o_date', '>=', self.from_date),
                ('o_date', '<=', self.to_date),
                ('state', '=', 'done')
            ])

        datas = {
            'ids': order_ids.ids,
            'model': 'hotel.restaurant.order',
            'form': order_ids.ids,

        }

        return {
            'type': 'ir.actions.report.xml',
            'report_name': 'hotel_restaurant.order_lines_report',
            'datas': datas,

        }

    @api.multi
    def action_get_report_table(self):
        order_ids = self.env['hotel.restaurant.order'].search(
            [
                ('o_date', '>=', self.from_date),
                ('o_date', '<=', self.to_date),
                ('state', '=', 'done')
            ],order='table_no , o_date')

        datas = {
            'ids': order_ids.ids,
            'model': 'hotel.restaurant.order',
            'form': order_ids.ids,

        }

        return {
            'type': 'ir.actions.report.xml',
            'report_name': 'hotel_restaurant.order_table_report',
            'datas': datas,

        }

